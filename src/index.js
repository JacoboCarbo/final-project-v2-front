import React from 'react';
import ReactDOM from 'react-dom';

import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {Router, Route, browserHistory} from 'react-router';
import reducer from './reducers';

import App from './components/App';
import NavComponent from './components/NavComponent';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import JourneyList from './components/JourneyList';
import AddJourney from './components/AddJourney';
import AddMoment from './components/AddMoment';
import JourneyIntro from './components/JourneyIntro';
import JourneyShow from './components/JourneyShow';


const store = createStore(reducer);

// If user logged in, redirect to journey list. Else, redirect to sign in / sign up page

ReactDOM.render(
    <Provider store={store}>
        <Router path="/" history={browserHistory}>
            <Route path="/home" component={App}/>
            <Route path="/signin" component={SignIn}/>
            <Route path="/signup" component={SignUp}/>
            <Route path="/journeys" component={JourneyList}/>
            <Route path="/journeys/new" component={AddJourney}/>
            <Route path="/journeys/:id" component={JourneyIntro}/>
            <Route path="/journeys/:id/moments" component={JourneyShow}/>
            <Route path="/journeys/:id/addMoment" component={AddMoment}/>
        </Router>
    </Provider>, document.getElementById('root')
)

ReactDOM.render(
    <NavComponent />, document.getElementById("navbar")
)