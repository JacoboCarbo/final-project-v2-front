import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Link, browserHistory} from 'react-router';

import {signedIn} from '../actions';
import { API_URL, USERS_URI, LOGIN_URI, API_HEADERS} from '../constants';

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            email: '',
            password: ''
        }
    }

    render() {
        return (
            <div className="form-horizontal" style={{margin: '5%'}}>
                <h2> Sign In </h2>
                <div className="form-group">
                    <input
                        className="form-control"
                        type="text"
                        placeholder="email"
                        onChange={event => this.setState({email: event.target.value})}
                    />
                    <input
                        className="form-control"
                        type="password"
                        style={{marginRight: '5px'}}
                        placeholder="password"
                        onChange={event => this.setState({password: event.target.value})}
                    />
                    <button
                        className="btn btn-primary"
                        type="button"
                        style={{marginRight: '5px'}}
                        onClick={() => this.signIn()}
                    >
                        Sign In
                    </button>
                    <div><Link to={'/signup'}> Sign up instead </Link></div>
                </div>
            </div>
        )
    }


    signIn() {
        let FETCH_URL = `${API_URL}${USERS_URI}${LOGIN_URI}`;
        let requestBody =  {email: this.state.email, password:this.state.password}
        fetch(FETCH_URL, {
            method: 'POST',
            headers: API_HEADERS,
            body: JSON.stringify(requestBody)
        })
        .then(response => {
            let user = response.json();
            this.setState({id: user.id});
            this.props.signedIn(user);
            if (response.ok) {
                browserHistory.push('/home');
            }
        })
        .catch(error => {
            this.setState({error: error});
            browserHistory.push('/signin');
        });
    }

}

function mapStateToProps(state) {
    const {user} = state;
    return {
        user
    }
}

export default connect(mapStateToProps, {signedIn})(SignIn);
