import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import '../stylesheets/home_page.css';

class App extends Component {

    constructor(props) {
        super(props)
        this.state = {
            state: null
        }
    }

    render() {
        return (
            <div className='home-page-component'>
                <h2>Welcome to Odyssey!</h2>
                <hr />
                <div className="home-page-link-wrapper">
                    <Link to="/journeys">
                        <div className="btn btn-large btn-success home-page-link">
                            Start
                        </div>
                    </Link>
                </div>
            </div>
        )
    }

    onStartClick() {
        console.log('state', this.state);
        if (this.props.user) {
            browserHistory.push('/journeys')
        }
        else {
            browserHistory.push('/signin')
        }
    }
}

function mapStateToProps(state) {
    return {}
}

export default connect(mapStateToProps, null)(App);
