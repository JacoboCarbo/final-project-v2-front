// React imports
import React, {Component} from 'react';
import ReactDOM from 'react-dom';

// Third-Party library imports
import L from 'leaflet';

// Project imports
import {MAP_CENTER, MAP_MAX_BOUNDS} from '../globals';
import '../stylesheets/journey-live-map.css';


class Map extends Component {

    constructor(props) {
        super(props);
        this.initializeMap = this.initializeMap.bind(this);
    }

    componentDidMount(){
        this.initializeMap();
        this.map.fitBounds(MAP_MAX_BOUNDS);
    }

    render(){
        return(
            <div id="map"></div>

        )
    }

    initializeMap() {
        this.map = L.map(ReactDOM.findDOMNode(this), {
            center: MAP_CENTER,
            maxBounds: MAP_MAX_BOUNDS,
            worldCopyJump: true,
            minZoom: 2.5,
            maxZoom: 18,
            layers: [
                L.tileLayer('https://api.tiles.mapbox.com/v4/jacobo-carbo.n383kbgh/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA')
            ]
        });

        var mapboxComic = L.tileLayer(
            'https://api.tiles.mapbox.com/v4/jacobo-carbo.n383kbgh/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA',
        );

        var mapboxPirate = L.tileLayer(
            'https://api.tiles.mapbox.com/v4/jacobo-carbo.n11960gd/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA',
        );

        var stamenWatercolor = L.tileLayer(
            'http://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png',
            {
                ext: 'png',
            }
        );

        var baseMaps = {
            "Pirate": mapboxPirate,
            "Watercolor": stamenWatercolor,
            "Comic": mapboxComic
        };

        L.control.layers(baseMaps).addTo(this.map);
    }
}

export default Map;