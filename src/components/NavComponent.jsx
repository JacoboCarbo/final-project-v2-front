import React, {Component} from 'react';
import { Navbar, Nav, NavItem, Glyphicon } from 'react-bootstrap';

class NavComponent extends Component {

    render() {
        return (
            <Navbar fixedTop="true">
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href={'/home'}>
                            <Glyphicon glyph="home"></Glyphicon> Odyssey
                        </a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav>
                    <NavItem eventKey={1} href={'/journeys'}>
                        <Glyphicon glyph="globe"></Glyphicon> Journeys
                    </NavItem>
                </Nav>
            </Navbar>
        )
    }
}

export default NavComponent;