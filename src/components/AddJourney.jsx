import React, {Component} from 'react';
// import {connect} from 'react-redux';
import {Link, browserHistory} from 'react-router';
import {API_URL, API_HEADERS} from '../constants';

import '../stylesheets/journey_list.css';

class addJourney extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: 'THIS STATE TITLE',
            start_datetime: null,
            end_datetime: null
        }
    }

    render() {
        return (
            <div className="add-journey-component">
                <div className="add-journey-header">
                    <div>
                        <Link to="/journeys">
                            <div
                                className="btn btn-danger back-button"
                                type="button"
                            >
                                Back
                            </div>
                        </Link>
                    </div>
                    <h2> Create a New Journey </h2>
                </div>
                <hr />
                <div className="form-horizontal">
                    <div className="form-group">
                        <div className="col-sm-2">
                            Title
                        </div>
                        <div className="col-sm-8">
                            <input
                                type="text"
                                placeholder="Title"
                                className="form-control add-journey-form-input"
                                onChange={event => this.setState({title: event.target.value})}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-2">
                            Start Date
                        </div>
                        <div className="col-sm-8">
                            <input
                                type="datetime-local"
                                placeholder="Start Date"
                                className="form-control add-journey-form-input"
                                onChange={event => this.setState({start_datetime: event.target.value})}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-2">
                            End Date
                        </div>
                        <div className="col-sm-8">
                            <input
                                type="datetime-local"
                                placeholder="End Date"
                                className="form-control add-journey-form-input"
                                onChange={event => this.setState({end_datetime: event.target.value})}
                            />
                        </div>
                    </div>
                    <button
                        className="btn btn-success"
                        type="button"
                        onClick={() => this.addJourney()}
                    >
                        Create
                    </button>

                </div>
            </div>
        )
    }

    addJourney() {
        console.log('this.state', this.state);
        let FETCH_URL = `${API_URL}journeys/`;
        let requestBody  = {
            user: '1',
            title: this.state.title,
            start_datetime: this.state.start_datetime,
            end_datetime: this.state.end_datetime,
        }

        fetch(FETCH_URL, {
            method: 'POST',
            headers: API_HEADERS,
            body: JSON.stringify(requestBody)
        })
            .then(response => response.json())
            .then(json => {
                console.log('response', json);
            });

        browserHistory.push(`/journeys/`);


    }
}


export default addJourney;
