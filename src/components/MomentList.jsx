import React, {Component} from 'react';
import MomentListItem from './MomentListItem';

import '../stylesheets/journey-live-map.css';


class MomentList extends Component {

    render() {
        return (
            <div className="col-sm-9 col-md-10 main">
                {
                    this.props.moments.map(moment => {
                        if (moment) {
                            return (
                                <MomentListItem key={moment.id} moment={moment}/>
                            )
                        }
                    })
                }
            </div>
        )
    }
}

export default MomentList;
