// React imports
import React, {Component} from 'react';
import { connect } from 'react-redux';
import {browserHistory} from 'react-router';

// Third-Party library imports
import $ from 'jquery';
import '../storymap';

// Project imports
import MomentList from './MomentList';
import {API_URL, JOURNEYS_URI} from '../constants';
import { getJourney, getJourneyMoments} from '../actions';
import '../stylesheets/journey-live-map.css';


class JourneyShow extends Component {

    constructor(props){
        super(props);
        this.onJourneyOverviewClick = this.onJourneyOverviewClick.bind(this);
        this.onAddMomentClick = this.onAddMomentClick.bind(this);

        this.state = {
            journey: null,
            moments: [],
            momentsLatLng: [],
            storyMapMarkers: []
        }
    }
    componentDidMount() {
        let FETCH_URL = `${API_URL}${JOURNEYS_URI}${this.props.params.id}`;
        fetch(FETCH_URL, {
            method: 'GET',
        })
        .then(response => response.json())
        .then(journey => {
            console.log('journey!', journey);

            const {id, title, origin, start_datetime, end_datetime} = journey;
            this.setState({journey: {id, title, origin, start_datetime, end_datetime}});
            this.props.getJourney({id, title, origin, start_datetime, end_datetime});

            let journeyMoments = [];
            let journeyMomentsLatLng = [];

            journeyMoments.push(journey.origin);
            journeyMomentsLatLng.push([journey.origin.latitude, journey.origin.longitude]);

            if (journey.waypoints) {
                journey.waypoints.forEach(waypoint => {
                    journeyMoments.push(waypoint);
                    journeyMomentsLatLng.push([waypoint.latitude, waypoint.longitude]);
                });
            }

            if (journey.destination) {
                journeyMoments.push(journey.destination);
                journeyMomentsLatLng.push([journey.destination.latitude, journey.destination.longitude]);
            }

            this.setState({moments: journeyMoments, momentsLatLng: journeyMomentsLatLng});
            this.props.getJourneyMoments(journeyMoments);
        })
        .then(() => {

            var storyMapMarkers = {};
            this.props.moments.forEach(moment => {
                storyMapMarkers[moment.id] = {lat: moment.latitude, lon: moment.longitude, zoom: 9};
            });

            this.setState({storyMapMarkers: storyMapMarkers});
            this.map = $('.main').storymap({markers: storyMapMarkers});
        });
    }

    componentWillUnmount(){
        this.map.remove();
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-9 col-md-7 main">
                        <div className="row">
                            <div className="col-sm-5 col-md-4">
                                <div className="btn btn-info back-button"
                                     type="button"
                                     onClick={this.onJourneyOverviewClick} >
                                    Journey Overview
                                </div>
                            </div>
                            <div className="col-sm-5 col-md-4">
                                <div className="btn btn-success back-button"
                                     type="button"
                                     onClick={this.onAddMomentClick} >
                                    Add Moment
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <MomentList moments={this.props.moments}/>
                        </div>

                    </div>

                    <div id="map" className="col-sm-3 col-md-5 sidebar">

                    </div>

                </div>
            </div>
        )
    }

    onJourneyOverviewClick(event) {
        browserHistory.push(`/journeys/${this.props.journey.id}/`);
    }

    onAddMomentClick(event) {
        browserHistory.push(`/journeys/${this.props.journey.id}/addMoment/`);
    }

}

function mapStateToProps(state) {
    const {journey, moments} = state;
    return {
        journey,
        moments
    }
}

export default connect(mapStateToProps, {getJourney, getJourneyMoments})(JourneyShow);
