// React imports
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
// import $ from 'jquery';

// Third-Party library imports
import L from 'leaflet';

// Project imports
import {API_URL, JOURNEYS_URI} from '../constants';
import {MAP_CENTER, MAP_MAX_BOUNDS} from '../globals';
import { getJourney, getJourneyMoments} from '../actions';
import '../stylesheets/journey-live-map.css';


class JourneyIntro extends Component {

    constructor(props){
        super(props);
        this.initializeMap = this.initializeMap.bind(this);
        this.onStartClick = this.onStartClick.bind(this);

        this.state = {
            journey: null,
            moments: [],
            momentsLatLng: [],
            markers: []
        }
    }

    componentDidMount() {
        let FETCH_URL = `${API_URL}${JOURNEYS_URI}${this.props.params.id}`;
        fetch(FETCH_URL, {
            method: 'GET',
        })
            .then(response => response.json())
            .then(journey => {

                const {id, title, origin, start_datetime, end_datetime} = journey;
                this.setState({journey: {id, title, origin, start_datetime, end_datetime}});
                this.props.getJourney({id, title, origin, start_datetime, end_datetime});

                let journeyMoments = [];
                let journeyMomentsLatLng = [];

                journeyMoments.push(journey.origin);
                journeyMomentsLatLng.push([journey.origin.latitude, journey.origin.longitude]);

                if (journey.waypoints) {
                    journey.waypoints.forEach(waypoint => {
                        journeyMoments.push(waypoint);
                        journeyMomentsLatLng.push([waypoint.latitude, waypoint.longitude]);
                    });
                }

                if (journey.destination) {
                    journeyMoments.push(journey.destination);
                    journeyMomentsLatLng.push([journey.destination.latitude, journey.destination.longitude]);
                }

                this.setState({moments: journeyMoments, momentsLatLng: journeyMomentsLatLng});
                this.props.getJourneyMoments(journeyMoments);
            })
            .then(() => {
                this.initializeMap();
                this.map.fitBounds(this.state.momentsLatLng);
                var currentZoom = this.map.getZoom();
                this.map.setZoom(currentZoom-1);

                var markers = [];
                this.props.moments.forEach(moment => {
                    markers.push(
                        L.marker([moment.latitude, moment.longitude])
                            .bindPopup(`<p>${moment.title}</p>`)
                    );
                });

                var polyline = L.polyline(this.state.momentsLatLng);

                L.layerGroup(markers)
                    .addLayer(polyline)
                    .addTo(this.map);

                this.setState({markers: markers});
            });
    }

    componentWillUnmount(){
        this.map.remove()
    }

    render() {
        return (
            <div className="map-wrapper" >
                <div id="map" className="live-map"></div>
                <div className="button-wrapper">
                    <div className="btn btn-large button-start-journey" type="button" onClick={this.onStartClick}> . </div>
                </div>
            </div>
        )
    }

    onStartClick(event) {
        browserHistory.push(`/journeys/${this.props.params.id}/moments`);
    }

    initializeMap() {
        this.map = L.map(ReactDOM.findDOMNode(this), {
            center: MAP_CENTER,
            maxBounds: MAP_MAX_BOUNDS,
            worldCopyJump: true,
            minZoom: 2.5,
            maxZoom: 18,
            layers: [
                L.tileLayer('https://api.tiles.mapbox.com/v4/jacobo-carbo.n383kbgh/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA')
            ]
        });

        var mapboxComic = L.tileLayer(
            'https://api.tiles.mapbox.com/v4/jacobo-carbo.n383kbgh/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA',
        );

        var mapboxPirate = L.tileLayer(
            'https://api.tiles.mapbox.com/v4/jacobo-carbo.n11960gd/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA',
        );

        var stamenWatercolor = L.tileLayer(
            'http://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png',
            {
                ext: 'png',
            }
        );

        var baseMaps = {
            "Pirate": mapboxPirate,
            "Watercolor": stamenWatercolor,
            "Comic": mapboxComic
        };

        L.control.layers(baseMaps).addTo(this.map);
    }
}

function mapStateToProps(state) {
    const {journey, moments} = state;
    return {
        journey,
        moments
    }
}

export default connect(mapStateToProps, {getJourney, getJourneyMoments})(JourneyIntro);
