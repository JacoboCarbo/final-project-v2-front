import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link, browserHistory} from 'react-router';
import JourneyListItem from './JourneyListItem';
import '../stylesheets/journey_list.css';

import {getJourneys} from '../actions';
import {API_URL, JOURNEYS_URI} from '../constants';


class JourneyList extends Component {

    constructor(props) {
        super(props);
        this.onJourneyClick = this.onJourneyClick.bind(this);
    }

    componentDidMount() {
        let FETCH_URL = `${API_URL}${JOURNEYS_URI}`;

        fetch(FETCH_URL, {
            method: 'GET',
        })
            .then(response => response.json())
            .then(journeys => {
                let journeys_list = [];
                journeys.forEach(journey => {
                    const {id, title} = journey;
                    journeys_list.push({id, title})
                });
                this.props.getJourneys(journeys_list);
            });
    }

    render() {
        return (
            <div className="journey-list-component">
                <h2 className="journey-list-header"> Your Journeys </h2>
                <hr />
                <div>
                    <Link to="/journeys/new">
                        <div
                            className="btn btn-success add-journey-button"
                            type="button"
                        >
                            New
                        </div>
                    </Link>
                </div>
                <div className="journey-list">
                    {
                        this.props.journeys.map(journey => {
                            if (journey) {
                                return (
                                    <JourneyListItem key={journey.id} journey={journey}
                                                     onClick={this.onJourneyClick}/>
                                )
                            }
                            return null;
                        })
                    }
                </div>
            </div>
        )
    }

    onJourneyClick(event) {
        browserHistory.push(`/journeys/${event}`);
    }
}

function mapStateToProps(state) {
    const {journeys} = state;
    return {
        journeys
    }
}

JourneyListItem.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

export default connect(mapStateToProps, {getJourneys})(JourneyList);
