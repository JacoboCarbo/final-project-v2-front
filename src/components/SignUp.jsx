import React, { Component } from 'react';
import { Link } from 'react-router';
import { API_URL, USERS_URI, REGISTER_URI } from '../constants';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: ''
    }
  }

  signUp() {
    let FETCH_URL = `${API_URL}${USERS_URI}${REGISTER_URI}`;
    const { email, password } = this.state;
    console.log('this.state', this.state);
    fetch(FETCH_URL, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({email: email, password: password})
    })
    .then(response => response.json())
    .then(json => {
      console.log('response.json', json);
      this.setState({email: json.email});
    })
    .catch(error => {
      this.setState({error:error});
    })
  }

  render() {
    return (
      <div className="form-inline" style={{margin: '5%'}}>
        <h2> Sign Up </h2>
        <div className="form-group">
          <input
            className="form-control"
            type="text"
            placeholder="email"
            onChange={event => this.setState({email: event.target.value})}
          />
          <input
            className="form-control"
            type="password"
            style={{marginRight: '5px'}}
            placeholder="password"
            onChange={event => this.setState({password: event.target.value})}
          />
          <button
            className="btn btn-primary"
            type="button"
            style={{marginRight: '5px'}}
            onClick={() => this.signUp()}
          >
            Sign Up
          </button>
          <div>{this.state.error.message}</div>
          <div><Link to={'/signin'}> Already a user? Sign in instead. </Link></div>
        </div>
      </div>
    )
  }
}

export default SignUp;
