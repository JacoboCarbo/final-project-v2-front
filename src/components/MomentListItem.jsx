import React, {Component} from 'react';
import {Grid, Row, Col, Image} from 'react-bootstrap';
import {BACKEND_URL} from '../constants';
import '../stylesheets/journey-live-map.css';

class MomentListItem extends Component {


    render(){
        if(this.props.moment.fileobjects){
            var imagePath = `${BACKEND_URL}${this.props.moment.fileobjects[0].media}`;
        }
        return (
            <section data-place={this.props.moment.id} key={this.props.moment.id}>
                <h2> {this.props.moment.title} </h2>
                <p> {this.props.moment.recorded_datetime} </p>
                <div className="moment-description">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lacinia odio ac est pellentesque, vel volutpat dolor molestie. Duis varius tempus pretium. In turpis quam, cursus non turpis vel, maximus fringilla ligula. In non elit a nulla pellentesque elementum. Nam ac odio nibh. Phasellus elit mauris, laoreet nec lorem aliquam, iaculis aliquam ante. Etiam eu libero justo. Duis eleifend, ipsum sed accumsan viverra, massa quam varius velit, ac sagittis diam enim sit amet mauris. Aenean rhoncus enim at nunc sodales finibus.</p>
                </div>
                <div className="moment-photos">
                    <Grid>
                        <Row>
                            <Col xs={6} md={4}>
                                <Image src={imagePath} rounded className="moment-photo"/>
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </section>
        )
    }
}

export default MomentListItem;

