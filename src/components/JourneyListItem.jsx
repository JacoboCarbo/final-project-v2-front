import React, { Component } from 'react';
import '../stylesheets/journey_list.css';


class JourneyListItem extends Component {

  render() {
    return (
      <div key={this.props.journey.id} className="journey-list-item" onClick={ () => {this.props.onClick(this.props.journey.id)}}>
        <div className="journey-list-item-title">
          {this.props.journey.title}
        </div>
      </div>
    )
  }
}

JourneyListItem.defaultTypes = {
  journey: {},
};

JourneyListItem.propTypes = {
  journey: React.PropTypes.object,
  onClick: React.PropTypes.func.isRequired
};

export default JourneyListItem;
