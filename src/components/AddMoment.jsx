import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Grid, Row, Col} from 'react-bootstrap';
import {browserHistory} from 'react-router';
import {API_URL, API_HEADERS, MOMENTS_URI, FILEOBJECTS_URI} from '../constants';

// Third-Party library imports
import $ from 'jquery';
import '../storymap';
import L from 'leaflet';


// Project imports
import {MAP_CENTER, MAP_MAX_BOUNDS} from '../globals';
import '../stylesheets/journey_list.css';
import '../stylesheets/journey-live-map.css';

class addMoment extends Component {

    constructor(props) {
        super(props);
        this.initializeMap = this.initializeMap.bind(this);
        this.addMoment = this.addMoment.bind(this);
        this.onMapClick = this.onMapClick.bind(this);
        this.onFileUpload = this.onFileUpload.bind(this);
        this.onBackClick = this.onBackClick.bind(this);

        this.state = {
            title: 'THIS STATE TITLE',
            recorded_datetime: null,
            latitude: null,
            longitude: null,
            file: null,
            imagePreviewUrl: null,
        }
    }

    componentDidMount() {
        this.map = $('.main').storymap({markers:{0: {lat:25, longitude:50}}});
        this.map.on('click', this.onMapClick);
    }

    componentWillUnmount(){
        this.map.remove();
    }


    render() {
        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (<img src={imagePreviewUrl} className="moment-photo" />);
        }

        return (
        <div className="container-fluid add-journey-component">
            <div className="row">
                <div className="col-sm-9 col-md-7 main">
                    <div className="add-journey-header">
                        <div
                            className="btn btn-danger back-button"
                            type="button"
                            onClick={this.onBackClick}
                        >
                            Back
                        </div>
                        <h2> Create a New Moment </h2>
                    </div>
                    <hr />

                    <div className="form-horizontal main">
                        <div className="form-group">
                            <div className="col-sm-2">
                                Title
                            </div>
                            <div className="col-sm-8">
                                <input
                                    type="text"
                                    placeholder="Title"
                                    className="form-control add-journey-form-input"
                                    onChange={event => this.setState({title: event.target.value})}
                                />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-2">
                                Date
                            </div>
                            <div className="col-sm-8">
                                <input
                                    type="datetime-local"
                                    placeholder="Datetime"
                                    className="form-control add-journey-form-input"
                                    onChange={event => this.setState({recorded_datetime: event.target.value})}
                                />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-2">
                                Latitude & Longitude
                            </div>
                            <div className="col-sm-4">
                                <input
                                    id="lat"
                                    type="latitude"
                                    placeholder="Latitude"
                                    className="form-control add-journey-form-input"
                                    onChange={event => this.setState({latitude: event.target.value})}
                                />
                            </div>
                            <div className="col-sm-4">
                                <input
                                    id="long"
                                    type="longitude"
                                    placeholder="Longitude"
                                    className="form-control add-journey-form-input"
                                    onChange={event => this.setState({longitude: event.target.value})}
                                />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-2">
                                Media
                            </div>
                            <div className="col-sm-8">
                                <input type="file" id="fileUpload" onChange={this.onFileUpload} />
                                {$imagePreview}
                            </div>
                        </div>
                        <button
                            className="btn btn-success"
                            type="button"
                            onClick={() => this.addMoment()}
                        >
                            Create
                        </button>
                        <div data-place={0}></div>
                    </div>
                </div>

                <div id="map" className="col-sm-3 col-md-5 sidebar">

                </div>

            </div>
        </div>
        )
    }

    onMapClick(event) {
        var newLat = event.latlng['lat'];
        var newLong = event.latlng['lng'];
        $('#lat').val(newLat);
        $('#long').val(newLong);
        this.setState({latitude: newLat, longitude: newLong});
    }

    addMoment() {
        var FETCH_URL = `${API_URL}${MOMENTS_URI}`;
        var requestBody  = {
            user: '1',
            journey: this.props.params.id,
            title: this.state.title,
            recorded_datetime: this.state.recorded_datetime,
            latitude: this.state.latitude,
            longitude: this.state.longitude
        };

        fetch(FETCH_URL, {
            method: 'POST',
            headers: API_HEADERS,
            body: JSON.stringify(requestBody)
        })
            .then(response => response.json())
            .then(json => {
                var FETCH_URL = `${API_URL}${FILEOBJECTS_URI}`;

                let momentId = json.id;
                console.log('json',json);

                var requestBody  = {
                    user: '1',
                    journey: this.props.params.id,
                    moment: momentId,
                    title: this.state.title,
                    recorded_datetime: this.state.recorded_datetime,
                    media: this.state.imagePreviewUrl
                }

                fetch(FETCH_URL, {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify(requestBody)
                })
                    .then(response => response.json())
                    .then(json => {
                        console.log('MEDIA response', json);
                    });
            });

        browserHistory.push(`/journeys/${this.props.params.id}`);


    }

    onFileUpload(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(file);
    }

    onBackClick() {
        browserHistory.push(`/journeys/${this.props.params.id}/moments`);
    }

    initializeMap() {
        this.map = L.map(ReactDOM.findDOMNode(this), {
            center: MAP_CENTER,
            maxBounds: MAP_MAX_BOUNDS,
            worldCopyJump: true,
            minZoom: 2.5,
            maxZoom: 18,
            layers: [
                L.tileLayer('https://api.tiles.mapbox.com/v4/jacobo-carbo.n383kbgh/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA')
            ]
        });

        var mapboxComic = L.tileLayer(
            'https://api.tiles.mapbox.com/v4/jacobo-carbo.n383kbgh/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA',
        );

        var mapboxPirate = L.tileLayer(
            'https://api.tiles.mapbox.com/v4/jacobo-carbo.n11960gd/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA',
        );

        var stamenWatercolor = L.tileLayer(
            'http://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png',
            {
                ext: 'png',
            }
        );

        var baseMaps = {
            "Pirate": mapboxPirate,
            "Watercolor": stamenWatercolor,
            "Comic": mapboxComic
        };

        L.control.layers(baseMaps).addTo(this.map);
        this.map.fitBounds(MAP_MAX_BOUNDS);
    }
}


export default addMoment;
