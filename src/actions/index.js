import { SIGNED_IN, GET_JOURNEYS, GET_JOURNEY, GET_JOURNEY_MOMENTS } from '../constants';

export function signedIn(user) {
  const action = {
    type: SIGNED_IN,
    user
  };
  return action;
}

export function getJourneys(journeys) {
  const action = {
    type: GET_JOURNEYS,
    journeys
  };
  return action
}

export function getJourney(journey) {
    const action = {
        type: GET_JOURNEY,
        journey
    };
    return action
}

export function getJourneyMoments(journeyMoments) {
    const action = {
        type: GET_JOURNEY_MOMENTS,
        journeyMoments
    };
    return action
}

// export function getStoryMapMarkers(storyMapMarkers) {
//     const action = {
//         type: GET_STORY_MAP_MARKERS,
//         storyMapMarkers
//     };
//     return action
// }