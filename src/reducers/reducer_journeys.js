import { GET_JOURNEYS } from '../constants';

export default (state = [], action) => {
    switch (action.type){
        case GET_JOURNEYS:
            const { journeys } = action;
            return journeys;
        default:
            return state;
    }
}
