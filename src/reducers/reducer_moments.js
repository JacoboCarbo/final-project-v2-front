import { GET_JOURNEY_MOMENTS } from '../constants';

export default (state = [], action) => {
    switch (action.type){
        case GET_JOURNEY_MOMENTS:
            const { journeyMoments } = action;
            return journeyMoments;
        default:
            return state;
    }
}
