import {SIGNED_IN} from '../constants';

let user = {
    id: null,
    email: null,
    first_name: null,
    last_name: null
}

export default (state = user, action) => {
    switch (action.type) {
        case SIGNED_IN:
            const {id, email, first_name, last_name} = action.user;
            user = {
                id: id,
                email: email,
                first_name: first_name,
                last_name: last_name
            };
            return user;
        default:
            return state;
    }
}
