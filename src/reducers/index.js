import { combineReducers } from 'redux';
import user from './reducer_user';
import journeys from './reducer_journeys'
import journey from './reducer_journey';
import moments from './reducer_moments';

export default combineReducers({
    user,
    journeys,
    journey,
    moments,
})
