import { GET_JOURNEY } from '../constants';

export default (state = [], action) => {
    switch (action.type){
        case GET_JOURNEY:
            const { journey } = action;
            return journey;
        default:
            return state;
    }
}
