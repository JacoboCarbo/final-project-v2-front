export const BACKEND_URL = 'http://localhost:8000';
export const API_URL = 'http://localhost:8000/api/1.0/';
export const USERS_URI = 'users/';
export const LOGIN_URI = 'login/';
export const REGISTER_URI = 'register/';
export const JOURNEYS_URI = 'journeys/';
export const MOMENTS_URI = 'moments/';
export const FILEOBJECTS_URI = 'file_objects/';
export const API_HEADERS = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

export const SIGNED_IN = 'SIGN_IN';

export const GET_JOURNEYS = 'GET_JOURNEYS';
export const GET_JOURNEY = 'GET_JOURNEY';
export const GET_JOURNEY_MOMENTS = 'GET_JOURNEY_MOMENTS';
// export const GET_STORY_MAP_MARKERS = 'GET_STORY_MAP_MARKERS';


