import L from 'leaflet';
import jQuery from 'jquery';
import _ from 'underscore';

import { MAP_CENTER, MAP_MAX_BOUNDS } from './globals';

(function ($) {

    $.fn.storymap = function(options) {

        var defaults = {
            selector: '[data-place]',
            breakpointPos: '33%',
            createMap: function () {
                var map = L.map('map', {
                    center: MAP_CENTER,
                    maxBounds: MAP_MAX_BOUNDS,
                    noWrap: true,
                    minZoom: 2.5,
                    maxZoom: 18,
                    layers: [
                        L.tileLayer('https://api.tiles.mapbox.com/v4/jacobo-carbo.n383kbgh/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA')
                    ]
                });

                var mapboxComic = L.tileLayer(
                    'https://api.tiles.mapbox.com/v4/jacobo-carbo.n383kbgh/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA',
                );

                var mapboxPirate = L.tileLayer(
                    'https://api.tiles.mapbox.com/v4/jacobo-carbo.n11960gd/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFjb2JvLWNhcmJvIiwiYSI6ImIwOTE3OGMwODhiYjc5YzZlZWNiODRmNzBiNTA4ZTJjIn0.dqABzsUJdLNg9xB5krzpMA',
                );

                var stamenWatercolor = L.tileLayer(
                    'http://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png',
                    {
                        ext: 'png',
                    }
                );

                var baseMaps = {
                    "Pirate": mapboxPirate,
                    "Watercolor": stamenWatercolor,
                    "Comic": mapboxComic
                };

                L.control.layers(baseMaps).addTo(map);
                map.fitBounds(MAP_MAX_BOUNDS);
                return map;
            }
        };

        var settings = $.extend(defaults, options);


        if (typeof(L) === 'undefined') {
            throw new Error('Storymap requires Laeaflet');
        }
        if (typeof(_) === 'undefined') {
            throw new Error('Storymap requires underscore.js');
        }

        function getDistanceToTop(elem, top) {
            var docViewTop = $(window).scrollTop();

            var elemTop = $(elem).offset().top;

            var dist = elemTop - docViewTop;

            var d1 = top - dist;

            if (d1 < 0) {
                return $(document).height();
            }
            return d1;

        }

        function highlightTopPara(paragraphs, top) {

            var distances = _.map(paragraphs, function (element) {
                var dist = getDistanceToTop(element, top);
                return {el: $(element), distance: dist};
            });

            var closest = _.min(distances, function (dist) {
                return dist.distance;
            });

            _.each(paragraphs, function (element) {
                var paragraph = $(element);
                if (paragraph[0] !== closest.el[0]) {
                    paragraph.trigger('notviewing');
                }
            });

            if (!closest.el.hasClass('viewing')) {
                closest.el.trigger('viewing');
            }
        }

        function watchHighlight(element, searchfor, top) {
            var paragraphs = element.find(searchfor);
            highlightTopPara(paragraphs, top);
            $(window).scroll(function () {
                highlightTopPara(paragraphs, top);
            });
        }

        var makeStoryMap = function (element, markers) {

            var topElem = $('<div class="breakpoint-current"></div>')
                .css('top', settings.breakpointPos);
            $('body').append(topElem);

            var top = topElem.offset().top - $(window).scrollTop();

            var searchfor = settings.selector;

            var paragraphs = element.find(searchfor);

            paragraphs.on('viewing', function () {
                $(this).addClass('viewing');
            });

            paragraphs.on('notviewing', function () {
                $(this).removeClass('viewing');
            });

            watchHighlight(element, searchfor, top);

            var map = settings.createMap();

            var initPoint = map.getCenter();
            var initZoom = map.getZoom();

            var fg = L.featureGroup().addTo(map);

            function showMapView(key) {

                fg.clearLayers();
                if (key === 'overview') {
                    map.setView(initPoint, initZoom, true);
                } else if (markers[key]) {
                    var marker = markers[key];
                    var layer = marker.layer;
                    if(typeof layer !== 'undefined'){
                        fg.addLayer(layer);
                    };
                    fg.addLayer(L.marker([marker.lat, marker.lon]));

                    map.setView([marker.lat, marker.lon], marker.zoom, 1);
                }

            }

            paragraphs.on('viewing', function () {
                showMapView($(this).data('place'));
            });
            return map
        };

        var map = makeStoryMap(this, settings.markers);

        return map;
    }

}(jQuery));